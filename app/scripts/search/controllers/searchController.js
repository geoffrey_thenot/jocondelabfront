'use strict';

angular.module('joconde.search')
	.controller('searchController', function ($scope, Notice) {		
		$scope.page = 1;
		$scope.arrayResults = [];
		$scope.label = '';
 		$scope.$watch('search', function(newValue, oldValue) { 
 			$scope.arrayResults = [];
 			$scope.results = [];
 			$scope.count = null;
 			$scope.page = 1;
 			$scope.label = '';
		  	if (newValue != oldValue) {
		  		$scope.noticesSearch = [];
		      	Notice.search($scope.search).then(function(documents) {
		      		angular.forEach(documents.data.hits.hits, function(hit, key){
		      			$scope.noticesSearch.push(hit.fields['_source.term.label']);
		      		});
		      		$scope.$broadcast('suggestChange',{"val":$scope.noticesSearch});
		      	});    	
		      	
		  	} 
		});

		$scope.$on('search', function(event, object) {
			event.preventDefault();
			$scope.label = object.item.label;
			Notice.getResults(object.item.label, $scope.page).then(function(documents) {
				$scope.count = documents.data.hits.total;
				$scope.results = _.chain(documents.data.hits.hits).map(function (doc) {
					if (doc._source.notice.titr)
						return {title: doc._source.notice.titr, id: doc._id};
				}).compact()._wrapped;
			});

			Notice.getEchoResults(object.item.label, 1).then(function(echos) {
				$scope.echoResults = echos;
				console.log(echos);
			});
		});

		$scope.more = function () {
			$scope.page++;
			Notice.getResults($scope.label, $scope.page).then(function(documents) {
				$scope.arrayResults.push(_.chain(documents.data.hits.hits).map(function (doc) {
					if (doc._source.notice.titr)
						return {title: doc._source.notice.titr, id: doc._id};
				}).compact()._wrapped);
			});
		};
	});
