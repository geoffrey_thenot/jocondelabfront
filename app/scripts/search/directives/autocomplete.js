'use strict';

angular.module('joconde.search')
    .directive('gtAutocomplete', function($timeout) {
        return {
            link: function(scope, element, attr) {
                scope.$on('suggestChange', function(event, notice) {
                    if (notice) {
                        element.autocomplete({
                            source: notice.val,
                            select: function(event, object) {
                                scope.$emit('search', object);
                               /* $timeout(function() {
                                    element.trigger('input');
                                }, 0);*/
                            },
                            messages: {
                                noResults: '',
                                results: function() {}
                            },
                            position: { 
                                my : "right top", 
                                at: "right bottom" 
                            },
                            appendTo: "#autocomplete-area",
                            minLength: 2
                        });
                    }
                });
            }
        }
    });