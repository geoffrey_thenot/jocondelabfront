'use strict';

angular.module('joconde.search')
    .directive('autoResize', function() {
        return {
            restrict:'A',
            link: function(scope, elem, attr) {
                var input = angular.element(elem).find('input');
                var fontSize = 60;
                var minWidth = 100;
                var size = 0;

                input.bind('keyup keydown blur update', function() {     
                    angular.element(elem).css('width', input.val().length * fontSize + minWidth);
                });
            }
        }
    });