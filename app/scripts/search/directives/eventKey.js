'use strict';

angular.module('joconde.search')
    .directive('gtSearch', function () {
        return {
            restrict:'E',
            replace:true,
            templateUrl: '/views/search/search.html',
            link: function (scope, element, attrs) {
                scope.isOpen = false;
                scope.element = element;
                var input = scope.element.find('input');

                var handler = function(event) {
                    if (event.which === 27) { // escape
                        scope.isOpen = false;
                        input.val('');
                        scope.search = '';
                        $('body').css('overflow', 'auto');

                    } else if (event == 0 || (event.which <= 90 && event.which >= 48)) { // others alphanumeric
                        input.focus(); // focus
                        scope.isOpen = true;
                        $('body').css('overflow', 'hidden');
                    }
                    scope.element.toggleClass('search-show', scope.isOpen);
                };

                $('body').bind('keydown keypress', handler);

                $('body').on('focus', 'form input', function() {
                    $("body").off('keydown keypress', handler);
                });

                $('body').on('blur', 'form input', function() {
                    $('body').bind('keydown keypress', handler);
                });

                scope.$on('open-search', function() {
                    handler(0);
                });

                scope.$on('$destroy', function() {
                    $("body").off('keydown keypress', handler);
                    $('body').css('overflow', 'auto');
                });

            },
            controller: function($scope) {
                $scope.close = function() {
                    $scope.isOpen = false;
                    $scope.element.toggleClass('search-show', false);
                };
            }
        }
});