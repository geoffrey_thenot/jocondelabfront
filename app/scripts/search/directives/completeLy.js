'use strict';

angular.module('joconde.search')
    .directive('completeLy', function($timeout) {
        return {
            link: function(scope, element, attr) {
                var input = document.getElementById('search');
                var p = completely(input, { 
                            fontSize:'32px',
                            promptInnerHTML:''
                        });
                p.onChange = function (text) {
                    p.startFrom = text.lastIndexOf(' ')+1;
                    p.repaint();
                    scope.search = text;
                };
                p.input.maxLength = 50;
                scope.$on('suggestChange', function(event, notice) {
                    if (notice) {
                        console.log(notice);
                        p.options = notice; 
                        p.startFrom = text.lastIndexOf(' ')+1;
                        p.repaint();
                    }
                });
            }
        }
    });