'use strict';

angular.module('joconde.search')
	.factory('Notice', function ($http, settings) {

        return {
        	search: function(val) {
        		return $http({method: 'GET', url: settings.apiUrl + '/autocomplete/' + val});
        	},
        	getResults: function(label, page) {
        		return $http({method: 'GET', url: settings.apiUrl + '/search/' + label, params: { filter: {p: page} }});
        	},
            getEchoResults: function(label, page) {
                return $http({method: 'GET', url: settings.apiUrl + '/search/echos/' + label, params: { filter: {p: page} }});
            }
        };
    });