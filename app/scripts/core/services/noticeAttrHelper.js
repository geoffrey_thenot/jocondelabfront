'use strict';

angular.module('joconde.core')

    .service('noticeAttrHelper', function ($translate, $rootScope) {
        var attrNames = {
            'aptn': $translate('NOTICE_APTN'),
            'attr': $translate('NOTICE_ATTR'),
            'autr': $translate('NOTICE_AUTR'),
            'dacq': $translate('NOTICE_DACQ'),
            'deno': $translate('NOTICE_DENO'),
            'dims': $translate('NOTICE_DIMS'),
            'drep': $translate('NOTICE_DREP'),
            'gene': $translate('NOTICE_GENE'),
            'hist': $translate('NOTICE_HIST'),
            'loca': $translate('NOTICE_LOCA'),
            'mill': $translate('NOTICE_MILL'),
            'paut': $translate('NOTICE_PAUT'),
            'peri': $translate('NOTICE_PERI'),
            'pins': $translate('NOTICE_PINS'),
            'repr': $translate('NOTICE_REPR'),
            'srep': $translate('NOTICE_SREP'),
            'tech': $translate('NOTICE_TECH'),
            'tico': $translate('NOTICE_TICO')
        };

        var attrDisplayed = {
            'aptn': true,
            'attr': true,
            'autr': true,
            'dacq': true,
            'deno': true,
            'dims': true,
            'drep': true,
            'gene': true,
            'hist': true,
            'loca': true,
            'mill': true,
            'paut': true,
            'peri': true,
            'pins': true,
            'repr': true,
            'srep': true,
            'tech': true,
            'tico': true
        };

        var formatArray = function (notice) {
            return _.chain(notice).pairs().map(function(value, key) {
                return value[1] != '' && value[0] != 'titr' ? {title: attrNames[value[0]], value: value[1], attr: value[0]} : false;
            }).compact()._wrapped;
        };

        return {
            get: function(notice) {
                return formatArray(notice);
            },
            toggle: function(attrName) {
                attrDisplayed[attrName] = !attrDisplayed[attrName];
            },
            isDisplayed: function(attrName) {
                return attrDisplayed[attrName];
            }
        };

    });