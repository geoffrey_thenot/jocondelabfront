'use strict';

angular.module('joconde.core')

    .service('imageHelper', function () {

        var ressourceUrl = 'http://www.culture.gouv.fr/Wave/image/joconde';

        var checkImage = function(pictures) {

            if(_.isUndefined(pictures)){
                return false;
            }
            var images = _.map(pictures.url, function (url, key) {
                if (url != null) return ressourceUrl + url;
            });
            return images;

        };

        return {
            get: function(pictures) {
                return checkImage(pictures);
            }
        };

    });