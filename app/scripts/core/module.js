'use strict';

angular.module('joconde.core', [])
	.config(function ($routeProvider) {
		$routeProvider
            .when('/echo', {
                templateUrl: 'views/layout/echo.html',
                controller: 'LastEchoCtrl'
            })
            .when('/echo/:id', {
                templateUrl: 'views/layout/echo.html',
                controller: 'EchoCtrl'
            })
            .when('/echos/archives', {
                templateUrl: 'views/layout/archives.html',
                controller: 'ArchivesEchoCtrl'
            })
            .when('/notices/:id', {
                templateUrl: 'views/layout/notice.html',
                controller: 'NoticeCtrl'
            })
            .when('/notices', {
                templateUrl: 'views/layout/annuaire.html',
                controller: 'AnnuaireCtrl'
            });
	});