'use strict';

angular.module('joconde.constant', [])
	.constant('settings', {
	    apiUrl: 'http://localhost:9001',
	    oauthUri: 'http://localhost:9001/oauth2/token'
	});