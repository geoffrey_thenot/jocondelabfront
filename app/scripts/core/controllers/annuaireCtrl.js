'use strict';

angular.module('joconde.core')
	.controller('AnnuaireCtrl', function () {
		$('body').on('click', '.directory', function() {
			var background = $('.directory').first().css('background');
			if (/^.*(annuaire0\.jpg).*$/.test(background)) {
				$('.directory').first().css('background', 'url(/images/annuaire1.jpg) top center no-repeat');
			} else {
				$('.directory').first().css('background', 'url(/images/annuaire0.jpg) top center no-repeat');
			}
		});
	});