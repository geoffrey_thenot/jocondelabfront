'use strict';

angular.module('joconde.core')
	.controller('EchoCtrl', function ($scope, $http, settings, $translate, $route, imageHelper) {
		$scope.id = $route.current.params.id;
		$scope.isFavorited = false;
		$scope.article = '1';
		$scope.lastEcho = null;

		$scope.loadEcho = true;

		$scope.get = function(id) {
			$http({method: 'GET', url: settings.apiUrl + '/echos/' + id, params: {lang: $translate.uses()}})
				.success(function(data) {
					$scope.lastEcho = data;
					$scope.wordSize = $scope.lastEcho.subject.split(' ')[0];
				})
				.finally(function() {
					$scope.loadEcho = false;
					$scope.isInFavorites($scope.id);
					$scope.notices = _.map($scope.lastEcho.coreNotice, function(notice) {
						return notice.id;
					});
					$http({method: 'GET', url: settings.apiUrl + '/notices', params: {notices: $scope.notices} })
						.success(function(data) {
							$scope.notices = formatNotices(data);
						});
				});
		};

		$scope.addToFavorites = function() {
			$http({method: 'post', url: settings.apiUrl + '/user/favorites' , params: {id: $scope.id, type: 2}})
				.success(function() {
					$scope.isFavorited = true;
				});
		};

		$scope.isInFavorites = function (id) {
			$http({method: 'get', url: settings.apiUrl + '/user/favorites/' + id, params: {type: 2}})
				.success(function(data) {
					$scope.isFavorited = data.favorite;
				});
		};

		$scope.removeFromFavorites = function() {
			$http({method: 'delete', url: settings.apiUrl + '/user/favorites/' + $scope.id , params: {type:2}})
				.success(function() {
					$scope.isFavorited = false;
				});
		};

		var calculMenu = function(num) {
			var start = num - 1;
			start = start > 0 ? start : 1;
			var array = [];
			for (var i = start; i < start + 4; i++) {
				array.push(i);
			}
			return array;
		};


        var formatNotices = function (notices) {
            return _.map(notices.docs, function (notice) {
                return {id: notice._id, title: notice._source.notice.titr, subtitle: notice._source.notice.autr, img: imageHelper.get(notice._source.image)[0]};
            });
        };


		$scope.get($scope.id);
		$scope.cursor = calculMenu($scope.id);


	});