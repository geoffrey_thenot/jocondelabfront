'use strict';

angular.module('joconde.core')
	.controller('HomeCtrl', function ($rootScope, $scope, $http, settings, $translate, $timeout) {
		$scope.ready = 'true';
		$scope.article = 1;
		$scope.lastEcho = null;
		$scope.sliderMain = $translate('SLIDER_MAIN');
		$scope.sliderEcho = $translate('SLIDER_ECHO');
		$scope.sliderSearch = $translate('SLIDER_SEARCH');

		$scope.params = {
			limit: 3,
			offset: 1,
			lang: $translate.uses()
		};
		$scope.echos = [];
		$scope.loadEcho = true;

		$http({method: 'GET', url: settings.apiUrl + '/echos', params: {limit: 1, lang: $translate.uses()}})
			.success(function(data) {
				$scope.lastEcho = data[0];
			})
			.finally(function() {
				$scope.loadEcho = false;
			});

		$scope.loadMore = function() {
			$scope.loadMoreEcho = true;
			$http({method: 'GET', url: settings.apiUrl + '/echos', params: $scope.params})
				.success(function(data) {
					angular.forEach(data, function(echo){
						$scope.echos.push(echo);
					});

					$scope.params.offset ++;
				})
				.finally(function() {
					$scope.loadMoreEcho = false;			
				});
		};

		$scope.openSearch = function () {
			$rootScope.$broadcast('open-search');
		};

		var slide = function() {
			if ($scope.article < 3) {
				$scope.article++;
			} else {
				$scope.article = 1;
			}
			$timeout(function() {
				slide();
			}, 5000);
		}
		$timeout(function() {
			slide();
		}, 5000);
	});