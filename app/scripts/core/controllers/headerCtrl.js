'use strict';

angular.module('joconde.core')
	.controller('HeaderCtrl', function ($rootScope, $scope, $timeout, $location) {
		$scope.account = false;
		$scope.help = false;
		$scope.color = false;
		$scope.logo = "/images/Logo_v2.png";
		$scope.$on('$routeChangeSuccess', function () {
			$scope.color = /^\/notices.*$/.test($location.path());
			$scope.logo = $scope.color ? "/images/Logo_v.png" : "/images/Logo_v2.png";
		});

		$scope.isConnected = false;

		$rootScope.$watch('accessToken', function(newVal) {
			console.log(newVal);
			if (newVal != null) {
				$scope.isConnected = true;
				$scope.account = false;
			} else {
				$scope.isConnected = false;
			}
		});


		$scope.showAccount = function () {
			$scope.help = false;
			$scope.account = !$scope.account;
		};

		$scope.showHelp = function () {
			$scope.account = false;
			$scope.help = !$scope.help;
		};

		$scope.openSearch = function () {
			$rootScope.$broadcast('open-search');
		};
		$timeout(function() {
			$scope.loading = true;
			$timeout(function() {
				angular.element('#intro').addClass('fade');
				$scope.loading = false;
				$timeout(function() {
					angular.element('#intro').animate({height: 'toggle'}, 2000, function() {
						angular.element('header').first().addClass('show');
						$('body').css('background-color', '#ffffff');
					});
				}, 2000);
			}, 3400);
	   	}, 800);

	})
	.animation('.help-animation', function() {
		return {
			enter: function(element, done) {
				element.slideDown();
			},
			leave: function(element, done) { 
				element.slideUp();
			}
		};
	})
	.animation('.account-animation', function() {
		return {
			enter: function(element, done) {
				element.show().find('.container').first().slideDown();
			},
			leave: function(element, done) { 
				element.find('.container').first().slideUp();
			}
		};
	});