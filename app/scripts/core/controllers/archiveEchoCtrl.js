'use strict';

angular.module('joconde.core')
	.controller('ArchivesEchoCtrl', function ($rootScope, $scope, $http, settings, $translate) {
		$scope.article = '1';

		$scope.params = {
			limit: 3,
			offset: 1,
			lang: $translate.uses()
		};
		$scope.echos = [];
		$scope.loadEcho = true;

		$http({method: 'GET', url: settings.apiUrl + '/echos', params: {limit: 100, lang: $translate.uses()}})
			.success(function(data) {
				$scope.echos = data;
			})
			.finally(function() {
				$scope.loadEcho = false;
			});

		$scope.loadMore = function() {
			$scope.loadMoreEcho = true;
			$http({method: 'GET', url: settings.apiUrl + '/echos', params: $scope.params})
				.success(function(data) {
					angular.forEach(data, function(echo){
						$scope.echos.push(echo);
					});

					$scope.params.offset ++;
				})
				.finally(function() {
					$scope.loadMoreEcho = false;			
				});
		};
	});