'use strict';

angular.module('joconde.core')
	.controller('NoticeCtrl', function ($scope, $http, settings, $translate, $route, imageHelper, noticeAttrHelper) {
		$scope.imageHelper = imageHelper;
		$scope.id = $route.current.params.id;
		$scope.isFavorited = false;
		$scope.noticeAttrHelper = noticeAttrHelper;
		$scope.get = function(id) {
			$scope.isLoading = true;
			$http({method: 'GET', url: settings.apiUrl + '/notices/' + id, params: {lang: $translate.uses()}})
				.success(function(data) {
					$scope.infos = noticeAttrHelper.get(data._source.notice);
					$scope.title = data._source.notice.titr;
					$scope.terms = data._source.term;
					if ($scope.title == '') {
						$scope.title = data._source.notice.deno;
					}
					$scope.title = $scope.title.toLowerCase();
					$scope.images = imageHelper.get(data._source.image);
				})
				.finally(function() {
					$scope.isLoading = false;
					$http({method: 'GET', url: settings.apiUrl + '/notices/'+ id +'/suggest' , 
						params: {terms: $scope.terms.name, lang: $translate.uses()}})
					.success(function(data) {
						$scope.suggests = _.chain(data.hits).map(function(hit) { 
							if (hit._id != id && hit._source.image.url != null) 
								return {id: hit._id, notice: hit._source};
						}).compact()._wrapped;
						$scope.noticeLoaded = true;
					});
				});
			$http({method: 'GET', url: settings.apiUrl + '/notices/' + id + '/echos', params: {lang: $translate.uses()}})
				.success(function(echo) {
					$scope.echo = echo;
				});
		};

		$scope.addToFavorites = function(id) {
			console.log("add");
			$http({method: 'post', url: settings.apiUrl + '/user/favorites' , params: {id: id, type: 1}})
				.success(function() {
					$scope.isFavorited = true;
				});
		};

		$scope.isInFavorites = function (id) {
			$http({method: 'get', url: settings.apiUrl + '/user/favorites/' + id, params: {type: 1}})
				.success(function(data) {
					$scope.isFavorited = data.favorite;
				});
		};

		$scope.removeFromFavorites = function(id) {
			$http({method: 'delete', url: settings.apiUrl + '/user/favorites/' + id , params: { type:1 }})
				.success(function() {
					$scope.isFavorited = false;
				});
		};

		$scope.hide = function(event) {
			//$(event.target).addClass('toggle').parent().parent().find('.hide-content').first().toggle();
			console.log($(event.target).attr('attribute'));
			noticeAttrHelper.toggle($(event.target).attr('attribute'));
		};

		$scope.get($scope.id);
		$scope.isInFavorites($scope.id);
	});