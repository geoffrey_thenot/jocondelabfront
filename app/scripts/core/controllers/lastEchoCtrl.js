'use strict';

angular.module('joconde.core')
	.controller('LastEchoCtrl', function ($scope, $http, settings, $translate) {
		$scope.isFavorited = false;
		$scope.article = '1';
		$scope.lastEcho = null;

		$scope.loadEcho = true;

		// load the last
		$http({method: 'GET', url: settings.apiUrl + '/echos', params: {limit: 1, lang: $translate.uses()}})
			.success(function(data) {
				$scope.lastEcho = data[0];
				$scope.wordSize = $scope.lastEcho.subject.split(' ')[0];
			})
			.finally(function() {
				$scope.loadEcho = false;
				$scope.isInFavorites($scope.lastEcho.id);
				$scope.id = $scope.lastEcho.id;
				$scope.cursor = calculMenu($scope.lastEcho.id);
			});


		$scope.addToFavorites = function() {
			$http({method: 'post', url: settings.apiUrl + '/user/favorites' , params: {id: $scope.lastEcho.id, type: 2}})
				.success(function() {
					$scope.isFavorited = true;
				});
		};

		$scope.isInFavorites = function (id) {
			$http({method: 'get', url: settings.apiUrl + '/user/favorites/' + id, params: {type: 2}})
				.success(function(data) {
					$scope.isFavorited = data.favorite;
				});
		};

		$scope.removeFromFavorites = function() {
			$http({method: 'delete', url: settings.apiUrl + '/user/favorites/' + $scope.lastEcho.id , params: {type:2}})
				.success(function() {
					$scope.isFavorited = false;
				});
		};
		var calculMenu = function(num) {
			var start = num - 1;
			start = start > 0 ? start : 1;
			var array = [];
			for (var i = start; i < start + 4; i++) {
				array.push(i);
			}
			return array;
		};

	});