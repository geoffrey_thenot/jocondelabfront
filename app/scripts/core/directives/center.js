angular.module('joconde.core')
	.directive('center', function($window) {
	    return {
	    	restrict: 'A',
	    	scope: {
	    		rotate: "@"
	    	},
	    	link: function(scope, element, attrs) {
	    		var handler = function() {
	    			var elemHeight;
	    			var elem;
	    			if (scope.rotate) {
	    				console.log("rotate");
	    				elemHeight = Math.cos((7*Math.PI)/4) * element.find('.get-size').first().width();
	    				elem = $('.rotate-subword');
	    			} else {
	    				elemHeight = element.height();
	    			}
		      	var screenHeight = $(window).height();

		      	var newTop = (screenHeight / 2) - (elemHeight);

		      	element.css('margin-top', newTop + 'px');
		      	if (elem) {
		      		elem.each(function() {
		      			this.css('margin-top', newTop + 'px').css('margin-left', newTop + 'px');
		      		});
		      	}
		      }

		      handler();
		      scope.$on('rotate', handler);
		      $($window).on('resize', handler);

	    	}
	    }
});