angular.module('joconde.core')
	.directive('rotateText', function() {
	    return {
	    	restrict: 'E',
	    	scope: {
	    		content: '='
	    	},
	    	link: function(scope, element, attrs) {
	    		scope.$watch('content', function() {
	    			if (scope.content != null ) {
			      		var wordArray = scope.content.split(' ');
			      		var position = 110;
			      		angular.forEach(wordArray, function(word, key) {
			      			var wordBlock = $('<div/>').
			      				addClass('rotate-subword').
			      				css({
			      					'top': position * key, 
			      					'left': position * key
			      				});
			      				angular.forEach(word.split(''), function(letter) {
			      				wordBlock.append($('<span/>').text(letter));
			      			});
			      			element.append(wordBlock);
			      		});
			      	}
		      	});
	    		scope.$emit('rotate');
	    		scope.$broadcast('rotate');
	    	}
	    }
 
});