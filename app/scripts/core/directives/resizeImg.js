angular.module('joconde.core')
	.directive('resizeImg', function ($window, $timeout) {
	    return {
	    	restrict: 'A',
	    	link: function(scope, element, attrs) {
	    		var handler = function () {
		      		var width =  attrs.width ?  attrs.width : element.parent().width();
		      		var maxHeight = attrs.height ? attrs.height : element.parent().height();
			        var newHeight, newWidth;
			        var img = new Image();
			       
			        
			        img.onload = function() { 
			            if( (width / this.width ) > (maxHeight / this.height) ) {
			            	newWidth = width;
			              	newHeight = Math.round( (width * this.height) / this.width);
			            } else {
			              	newHeight = maxHeight;
			              	newWidth = Math.round( (maxHeight * this.width) / this.height);
			            }

			            element.parent().css({
			               'background-image': 'url(' + attrs.ngSrc + ')',
			               'background-size': newWidth + 'px ' + newHeight + 'px',
			               'width': '100%',
			               'height': maxHeight + 'px',
			               'display': 'block',
			               'background-position': 'center center',
			               'background-repeat': 'no-repeat'
			            });
			            element.css('display', 'none');
		 
			        };
		 					
			        img.src = attrs.ngSrc;

			    }
			    handler();
			    //$timeout(function() {handler();}, 10);
			    angular.element($window).bind('resize', handler);
	    	}
	    }
 
});