angular.module('joconde.core')
	.directive('resize', function() {
	    return {
	    	restrict: 'A',
	    	link: function(scope, element, attrs) {
	      		var divided = attrs['divided'];
		        var img = new Image();
		       
		        
		        img.onload = function() { 
		        	this.width = this.width / divided;
		        	this.height = this.height / divided;

		            element.parent().css({
		               'width': this.width + 'px',
		               'height': this.height + 'px',
		               'display': 'block'
		            });
		        };
	 
		        img.src = attrs.ngSrc;
	    	}
	    }
 
});