'use strict';

angular.module('joconde.core')
    .directive('menuTranslate', function () {
        return {
            restrict:'E',
            templateUrl: '/views/core/directives/translate.html',
            link: function(scope, elem, attrs) {
                var menu = elem.find('.dropdown-menu');
                elem.find('.dropdown-toggle').on('click', function(event) {
                    event.stopPropagation();
                    menu.toggle();
                });

                angular.element('body').on('click', function(){
                    menu.hide();
                });
            },
            controller: 'MenuTranslateCtrl'
        }
    })
    .controller('MenuTranslateCtrl', function ($scope, $translate) {
        $scope.languages = ['fr','en', 'es', 'de'];
        $scope.currentLanguage = $translate.uses();

        $scope.change = function(language) {
            $translate.uses(language);
            $scope.currentLanguage = $translate.uses();
        };
    });