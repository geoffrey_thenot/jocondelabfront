'use strict';

function TermsCtrl($scope, Terms) {
 	$scope.terms = Terms.query();
	$scope.$watch('search', function(newValue, oldValue) { 
	  if (newValue != oldValue) {
	      $scope.firstPage();
	  } 
	});
 	$scope.orderProp = 'id';
}
