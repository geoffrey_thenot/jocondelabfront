'use strict';

angular.module('jocondeFrontApp', [
        'ngRoute',
        'ngResource', 
        'ngCookies',
        'ngSanitize', 
        'ngAnimate',
        'paginator',
        'pascalprecht.translate',
        'joconde.search',
        'joconde.constant',
        'joconde.core',
        'joconde.users'
    ])
    .config(['$routeProvider', '$locationProvider', '$translateProvider', function ($routeProvider, $locationProvider, $translateProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'views/layout/home.html',
                controller: 'HomeCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
 




        $translateProvider
            .translations('fr', {
                'NAME': 'Joconde-Lab',
                'MENU_ECHO': 'Écho',
                'MENU_NOTICES': 'Notices',
                'MENU_SEARCH': 'Recherche',
                'MENU_HELP': 'Comment oeuvrer dans le site?',
                'MENU_ACCOUNT': 'Mon Compte',
                'LOGIN_TITLE': 'Sauvegardez et partagez vos échos et aricles favoris en rejoignant librement la communauté Joconde-Lab.',
                'LOGIN_JOCONDE': 'Avec votre compte Joconde',
                'LOGIN_SOCIAL': 'Avec le réseau de votre choix',
                'OR': 'Ou',
                'PLACEHOLDER_MAIL': 'Votre adresse de messagerie',
                'PLACEHOLDER_PWD': 'Votre mot de protection',
                'PLACEHOLDER_CONFIRM': 'Confirmer le mot de protection',
                'BUTTON_LOGIN': 'Connectez-vous maintenant',
                'ECHO_TITLE': 'échos',
                'ECHO_NUM': 'Numéro',
                'ECHO_NUMBER': 'Echo numéro {{ value }}',
                'ECHO_READ': 'Lire maintenant >',
                'ECHO_ABOUT': 'à propos de l\'auteur',
                'ECHO_MORE': 'Vous voulez en voir davantage?',
                'ECHO_ALL': 'Accéder aux archives'   ,
                'ECHO_NOTICES': 'Les œuvres à consulter',
                'REGISTER_JOCONDE': 'Inscrivez-vous à Joconde',
                'REGISTER_BTN': 'Valider l\'inscription',
                'SWITCH_CONNECT': 'Se connecter',
                'SWITCH_REGISTER': 'S\'enregistrer',
                'NOTICE_APTN':'Anciennes appartenances',
                'NOTICE_ATTR':'Anciennes attributions',
                'NOTICE_AUTR':'Auteur',
                'NOTICE_DACQ':'Date d\'acquisition',
                'NOTICE_DENO':'Dénomination',
                'NOTICE_DIMS':'Dimensions',
                'NOTICE_DREP':'Date de la représentation',
                'NOTICE_GENE':'Genèse',
                'NOTICE_HIST':'Historique',
                'NOTICE_LOCA':'Lieu de conservation',
                'NOTICE_MILL':'Datation',
                'NOTICE_PAUT':'Précisions',
                'NOTICE_PERI':'Période de création',
                'NOTICE_PINS':'Inscriptions',
                'NOTICE_REPR':'Représentation',
                'NOTICE_SREP':'Source de la représentation',
                'NOTICE_TECH':'Techniques utilisés',
                'NOTICE_TICO':'Type de l\'objet',
                'NOTICE_TITR':'Titre',
                'NOTICES_OTHERS': 'Les notices qui pourraient également vous intéresser',
                'DASHBOARD_NOTES': 'Prises de notes',
                'DASHBOARD_FOLDER': 'Dossiers',
                'DASHBOARD_ADDFOLDER': 'Ajouter un dossier',
                'DASHBOARD_IMP': 'Impressionnisme',
                'DASHBOARD_SCULPTURE': 'Sculpture',
                'DASHBOARD_SETTINGS': 'Déconnexion',
                'DASHBOARD_SAVES': 'Vos {{ value }} sélectionnées',
                'DASHBOARD_SAVES_H': 'Vos {{ value }} sélectionnés',
                'DASHBOARD_DISPLAY': 'Type d\'affichage:',
                'DASHBOARD_VISUAL': 'Visuel',
                'DASHBOARD_TEXT': 'Textuel',
                'SLIDER_SEARCH': 'Chercher à tout moment sur Joconde-Lab une oeuvre, un courant artistique ou encore un écho en particulier. <br />Pour celà, il vous suffit simplement de commencer à écrire sur votre clavier ce que vous recherchez pour faire apparaitre le menu.<br />Vous pouvez également passer par le répertoire de notices par la barre de navigation. <br />Essayez !',
                'SLIDER_ECHO': 'Écho est une réflexion hebdomadaire rédigée par un professionnel du monde de l’art autour d’une œuvre ou alors d’un thème en regroupant plusieurs. <br /> Ce contenu rédactionnel éxigeant a pour soutient la base de donnée Joconde, mise en place par le ministère de la culture, pour vous permettre d’approfondir vos connaissances.',
                'SLIDER_MAIN': 'Joconde-Lab est un média œuvrant pour le partage et la découverte des trésors présent dans l’ensemble de nos musées français.<br />Au travers d’échos, le ministère de la culture, en partenariat avec une revue d’art exigeante, propose des contenus rédigés présentant une réflexion artistique autour d’une œuvre ou bien d’un thème en regroupant plusieurs.Vous pouvez également sauvegardez vos notices d’œuvres ou échos préférés par le biais d’un compte sur le site Joconde-Lab.<br />Grâce à votre espace, vous pourrez accéder à vos sélections de n’importe quel support: mobile, tablette ou ordinateur ! <br />Partagez vos découvertes avec vos amis !',
                'SLIDER_BTN_MAIN': 'Découvrez le site avec une vidéo',
                'SLIDER_BTN_ECHO': 'Accéder au dernier écho écrit',
                'SLIDER_BTN_SEARCH': 'Vous connaissez le nom exact?',
                'SLIDER_BTN_HELP': 'Vous nécéssitez un peu d\'aide?',
                'FAVORITE_REMOVE': 'Notice ajoutée!',
                'FAVORITE_ADD': 'Ajouter la notice',
                'FAVORITE_REMOVEE': 'Echo ajoutée!',
                'FAVORITE_ADDE': 'Ajouter l\'écho',
                'ECHO_AUTEUR_MORE': 'Voir les autres échos de cet auteur >'
            });
        $translateProvider
            .translations('en', {
                'MENU_ECHO': 'Gossip',
                'MENU_NOTICES': 'Notices',
                'MENU_SEARCH': 'Search',
                'MENU_HELP': 'How it works?',
                'MENU_ACCOUNT': 'My account',
                'LOGIN_TITLE': 'Save and share your favorites articles or echos by joining Joconde-Lab\'s community.',
                'LOGIN_JOCONDE': 'With your Joconde account',
                'LOGIN_SOCIAL': 'With your favorite network',
                'OR': 'Or',
                'PLACEHOLDER_MAIL': 'Your mail adress',
                'PLACEHOLDER_PWD': 'Your secret secure word',
                'BUTTON_LOGIN': 'Connect now',
                'ECHO_TITLE': 'Gossip',
                'ECHO_NUMBER': 'Gossip number {{ value }}',
                'ECHO_READ': 'Read now >',
                'ECHO_ABOUT': 'About the autor',
                'ECHO_MORE': 'Do you want to see more?',
                'ECHO_ALL': 'Access to archives',
                'ECHO_NOTICES': 'Notices to read'     
            });
 
        $translateProvider.preferredLanguage('fr');
        $translateProvider.useLocalStorage();

    }])
    .run(function ($rootScope, $location) {
        $('body').css('background-color', '#000000');
        // enumerate routes that need authentication
        var routesRequireAuth = ['/user/dashboard'];

        // check if current location matches route  
        var routeClean = function (route) {
            return _.find(routesRequireAuth,
                function (authRoute) {
                    return _.str.startsWith(route, authRoute);
                });
        };

        // check if auth is require and if the user is logged, else redirect to home
        $rootScope.$on('$routeChangeStart', function() {
            if(routeClean($location.url()) && !$rootScope.accessToken){
                $location.path('/');
            }
        });

    });

