'use strict';

angular.module('joconde.users', [])

    .config(function ($routeProvider, $httpProvider) {

        $routeProvider
	        .when('/user/register', {
	            templateUrl: 'views/users/register.html',
	            controller: 'RegisterCtrl',
	        })
            .when('/user/login', {
                templateUrl: 'views/users/login.html',
                controller: 'LoginCtrl',
            })
            .when('/user/:id/edit', {
                templateUrl: 'views/users/edit.html',
                controller: 'UserEditCtrl'
            })
            .when('/user/:id/edit/password', {
                templateUrl: 'views/users/edit-password.html',
                controller: 'UserEditPasswordCtrl'
            })
            .when('/user/dashboard', {
                templateUrl: 'views/users/dashboard.html',
                controller: 'DashboardCtrl',
            });

        var interceptor = ['$rootScope', '$q', function(scope, $q) {

            function success(response) {
                return response;
            };

            function error(response) {
                if (response.status == 401) {
                    var deferred = $q.defer();
                    scope.$broadcast('event:unauthorized');    
                    return deferred.promise;
                }

                return $q.reject( response );
            };

            return function(promise) {
                return promise.then(success, error);
            };

        }];
        $httpProvider.responseInterceptors.push(interceptor);
    })

    .run(['$rootScope', '$http', 'TokenHandler', 'settings', '$location', function (scope, $http, tokenHandler, settings) {

        if (localStorage.accessToken) {
            tokenHandler.set(localStorage.accessToken);
        }

        scope.$on('event:authenticate', function (event, username, password) {
            var payload = {
                username: username,
                password: password,
                grant_type: 'password',
                client_id: 'id',
                client_secret: 'secret' 
            };

            var config = {
                headers : {
                    'Authorization': 'basic none',
                    'Accept': 'application/json;odata=verbose'
                }
            };
                
            $http.post(settings.oauthUri, payload, config)
                .success(function(data) {
                    tokenHandler.set(data.access_token);
                    scope.$broadcast('event:authenticated');
                });
            });
        }
    ]);