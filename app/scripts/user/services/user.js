'use strict';

angular.module('joconde.users')
	.factory('Users', function($http, settings) {
	    return {
	    	create: function(user) {
	    		return $http({method: 'POST', url: settings.apiUrl + '/user', params: user});
	    	},
	    	get: function(id) {
	    		return $http({method: 'GET', url: settings.apiUrl + '/user/' + id});
	    	}
	    }
	});