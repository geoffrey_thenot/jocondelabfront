'use strict';

angular.module('joconde.users')
	.factory('TokenHandler', function($http, $rootScope) {
	  	var tokenHandler = {};
	  	var token = null;

	  	tokenHandler.set = function(newToken) {
	  		$http.defaults.headers.common.Authorization = 'Bearer ' + newToken;
	  		localStorage.accessToken = newToken;
	  		$rootScope.accessToken = newToken;
	    	token = newToken;
	  	};

	  	tokenHandler.get = function() {
	    	return token;
	  	};

	  	tokenHandler.remove = function() {
	  		localStorage.removeItem('accessToken');
	  		$rootScope.accessToken = null;
	  		this.token = null;
	  	};

	  	return tokenHandler;
	});