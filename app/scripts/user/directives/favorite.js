'use strict';

angular.module('joconde.users')

    .directive('favorite', function () {

        return {
            restrict: 'E',
            scope: {
                objectId: '=id'
            },
            templateUrl: 'views/users/directives/favorite.html',
            controller: 'FavoriteButtonCtrl'
        };
    })

    .controller('FavoriteButtonCtrl', function ($scope, $http, settings, TokenHandler) {

        $scope.favorite = function(id) {
            $http.post(settings.apiUrl + '/user/favorites', {id: id, access_token: TokenHandler.get()}).then(function() {

            });
        };
    });
