'use strict';

angular.module('joconde.users')
	.directive('userLogin', function() {
	  	return {
	    	restrict: 'E',
		    templateUrl: 'views/users/directives/login.html',
		    link: function(scope, element, attrs) {
		    	scope.account = 'login';
		    	scope.show = true;
		      	scope.$on('event:unauthorized', function(event) {
		        	scope.show = true;
		      	});

		      	scope.$on('event:authenticated', function(event) {
		        	scope.show = false;
		        	scope.account = false;
		        	console.log(scope);
		      	});
		      	
		    },
		    controller: 'UserLoginCtrl'
	  	}
	})
	.controller('UserLoginCtrl', function ($scope, Users) {

		$scope.switchView = function() {
			$scope.account = $scope.account == 'login' ? 'register' : 'login';
		};

		$scope.newUser = {
            email: '',
            password: '',
            confirm: ''
        };

        $scope.user = {
            email: '',
            password: ''
        };

        var check = function () {
        	if ($scope.newUser.email != '' && $scope.newUser.password != ''
				&& ($scope.newUser.password == $scope.newUser.confirm))
					return true;

			return false
        };
        
        $scope.submit = function () {
        	if (check()) {
        		Users.create($scope.newUser).then(function() {
                    $scope.success = true;
                    $scope.message = "Votre enregistrement a été fait avec succès !";  
                });
        	}
        };

        $scope.login = function() {
        	$scope.$emit('event:authenticate', $scope.user.email, $scope.user.password);
        };
	});