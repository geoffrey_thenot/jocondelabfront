'use strict';

angular.module('joconde.users')

    .directive('active', function () {

        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.on('click', function() {
                    $(this).toggleClass('active');
                    $(this).next('ul').toggle('slow');
                });
            }
        };
    })