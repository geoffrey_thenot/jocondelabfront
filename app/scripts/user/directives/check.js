'use strict';

angular.module('joconde.users')

	.directive('passwordCheck', function () {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, elem, attrs) {
				var divError = angular.element('#' + attrs.error);
				var firstPassword =  angular.element('#' + attrs.passwordCheck);
				var submit = angular.element('#' + attrs.submit);
				var checked = false;
				angular.element(elem).on('keyup', function () {
					scope.$apply(function () {
						checked = elem.val() === firstPassword.val();
						if (checked) {
							scope.checkPassword = null;
							submit.removeClass('disabled');
							divError.removeClass('error');

						} else {
							scope.checkPassword =  'Les mots de passes sont différents';
							submit.addClass('disabled', true);
							divError.addClass('error');
						}
					});
				});
			}
		};
	});