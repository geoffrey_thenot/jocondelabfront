'use strict';

angular.module('joconde.users')

    .controller('RegisterCtrl', function ($scope, $resource, Users) {

        $scope.newUser = {
            email: '',
            password: '',
            confirm: ''
        };

        var check = function () {
        	if ($scope.newUser.email != '' && $scope.newUser.password != ''
				&& ($scope.newUser.password == $scope.newUser.confirm))
					return true;

			return false
        };
        
        $scope.submit = function () {
        	if (check()) {
        		Users.create($scope.newUser).then(function() {
                    $scope.success = true;
                    $scope.message = "Votre enregistrement a été fait avec succès !";  
                });
        	}
        };

    });