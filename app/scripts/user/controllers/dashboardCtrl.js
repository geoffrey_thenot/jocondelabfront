'use strict';

angular.module('joconde.users')

    .controller('DashboardCtrl', function ($scope, $resource, settings, $http, imageHelper, TokenHandler, $location) {

    	$scope.displayText = true;
    	$scope.displayView = 'notices';
    	$scope.displayFolder = '';
        $scope.height = 120;
    	$scope.favorites = [];

    	$scope.switchDisplay = function () {
    		$scope.displayText = !$scope.displayText;
    	};

    	$scope.switchView = function (view) {
    		$scope.displayView = view;
            switch (view) {
                case 'notices' : 
                    $scope.favorites = $scope.formatedNotices; 
                    $scope.height = 120;
                    break;
                case 'echo' : 
                    $scope.favorites = $scope.formatedEchos; 
                    $scope.height = 155;
                    break;
            }
    	};

    	$scope.switchFolder = function (id) {
    		$scope.displayFolder = id;
    	};

    	$scope.loadFavorites = function () {
    		$scope.isLoading = true;
			$http({method: 'GET', url: settings.apiUrl + '/user/favorites'})
				 .success(function(data) {
                    $http({method: 'GET', url: settings.apiUrl + '/user/echos/favorites',
                        params: {echos: _.chain(data).where({type: 2}).map(function(value) {return value.object_id;} )._wrapped }})
                        .success(function(echos) {
                            $scope.formatedEchos = formatEchos(echos);
                        });
                    $http({method: 'GET', url: settings.apiUrl + '/user/notices/favorites',
                        params: {notices: _.chain(data).where({type: 1}).map(function(value) {return value.object_id;} )._wrapped }})
                        .success(function(notices) {
                            $scope.formatedNotices = formatNotices(notices);
                            $scope.favorites = $scope.formatedNotices;
                        });
				})
				.finally(function() {
					$scope.isLoading = false;
				});

    	};

        $scope.disconnect = function() {
            TokenHandler.remove();
            $location.path("/#");
        };

        var formatNotices = function (notices) {
            return _.map(notices.docs, function (notice) {
                return {id: notice._id, title: notice._source.notice.titr, subtitle: notice._source.notice.autr, description: '', img: imageHelper.get(notice._source.image)[0]};
            });
        };

        var formatEchos = function (echos) {
            return _.map(echos, function (echo) {
                /* dirty hack ...*/
                var img = echo.image.split('/');
                return {id: echo.id, title: echo.title, subtitle: echo.about_title, description: echo.intro, img: '/images/echos/' + img[img.length -1]};
            });
        };

    	$scope.loadFavorites();

    });	