Joconde lab Front APP
---------------------

# Install dev mode 
- `git clone https://bitbucket.org/geoffrey_thenot/jocondelabfront.git`
- `npm install`
- `bower install`
- `grunt server`
- go at http://localhost:9000

- Configure serveur domain name in file app/scripts/core/constants/constants.js (default localhost:9001)

